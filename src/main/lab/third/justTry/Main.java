package main.lab.third.justTry;

import main.lab.third.justTry.util.random.RandomDoubleGenerator;
import main.lab.third.justTry.util.test.TestRunner;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        RandomDoubleGenerator generator = new RandomDoubleGenerator(0, 10);


        int size = 5;
        int k = 1;
        SystemModel model = new SystemModel(generator, size, k);
        model.prepareVectorF();

        SystemSolver solver = new SystemSolver(model);
        System.out.println(solver.getSolution());

    }
}
