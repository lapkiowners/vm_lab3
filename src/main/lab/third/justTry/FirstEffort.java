package main.lab.third.justTry;

import Errors.ImpossibleToPutDirtyVectorException;
import Errors.TinyMatrixException;
import main.lab.third.justTry.MatrixSolution.MatrixRow;
import main.lab.third.justTry.util.TripleRunner;
import main.lab.third.justTry.util.dataparser.DataParser;
import main.lab.third.justTry.util.random.RandomDoubleGenerator;

import java.util.ArrayList;
import java.util.List;

public class FirstEffort {
    // матрица квадратная, потому достаточно 1 переменной
    private int size;

    private int indexOfRowP;
    private int indexOfRowQ;                //хотя оно и будет вычисляться, как indexOfRowP + 2

    private List<Double> vectorA;
    private List<Double> vectorB;
    private List<Double> vectorC;
    private List<Double> vectorF;
    private List<Double> vectorOfOnes;
    private List<Double> vectorP;
    private List<Double> vectorQ;

    private List<Double> solveVector;
    private List<Double> solveVectorInOnes;

    private int generateFrom = -1;
    private int generateTo = 1;

    private final RandomDoubleGenerator generator;

    private boolean isDebug = false;

    public FirstEffort(String filename) {
        this(DataParser.InputFromFile(filename).getSize(),
                DataParser.InputFromFile(filename).getIndexOfRowP());
    }

    public FirstEffort(int size, int indexK) {

        // TODO: добавить проверку на дурака (стоит ли?)

        checkStartingValidity(size, indexK);

        this.size = size;
        this.indexOfRowP = indexK;
        this.indexOfRowQ = indexK + 2;
        generator = new RandomDoubleGenerator(generateFrom, generateTo);

        prepareVectors();
    }

    public FirstEffort(int size, int indexK, int generateFrom, int generateTo) {
        this.generateFrom = generateFrom;
        this.generateTo = generateTo;

        checkStartingValidity(size, indexK);

        this.size = size;
        this.indexOfRowP = indexK;
        this.indexOfRowQ = indexK + 2;
        generator = new RandomDoubleGenerator(generateFrom, generateTo);

        prepareVectors();
    }

    public void setIsDebug(boolean isDebug) {
        this.isDebug = isDebug;
    }

    private void checkStartingValidity(int size, int indexK) {
        if (size < 4) {
            throw new TinyMatrixException();
        }

        if (size < indexK + 2) {
            throw new ImpossibleToPutDirtyVectorException("P");
        }

        if (size < indexK) {
            throw new ImpossibleToPutDirtyVectorException("Q");
        }
    }

    private List<Double> generateRandomVector(int size) {
        // TODO: реализовать генератор вектора случайных чисел
        return generator.getRandomList(size);
    }

    private List<Double> generateOneFilledVector(int size) {
        return generator.getFilledOneList(size);
    }

    private List<Double> generateZeroFilledVector(int size) {
        return generator.getFilledZeroList(size);
    }


    private void prepareVectors() {
        prepareVectorA();
        prepareVectorB();
        prepareVectorC();

        prepareVectorP();
        prepareVectorQ();

        prepareVectorF();
        prepareVectorOfOnes();
    }

    private void prepareVectorA() {
        vectorA = new ArrayList<>();
        vectorA.add(0.0);           // в векторе А нет нулевого элемента, добавлено искусственно ради нормальной нумерации
        vectorA.addAll(generateRandomVector(size - 1));
    }

    private void prepareVectorB() {
        vectorB = new ArrayList<>();
        vectorB.addAll(generateRandomVector(size - 1));
        vectorB.add(0.0);           // в векторе В нет n-ого элемента, добавлено искусственно ради нормальной нумерации
    }

    private void prepareVectorC() {
        vectorC = new ArrayList<>();
        vectorC.addAll(generateRandomVector(size));
    }

    private void prepareVectorP() {
        vectorP = new ArrayList<>();
        vectorP.addAll(generateRandomVector(indexOfRowP - 1));

        vectorP.add(vectorA.get(indexOfRowP));
        vectorP.add(vectorC.get(indexOfRowP));
        vectorP.add(vectorB.get(indexOfRowP));

        this.vectorP.addAll(generateRandomVector(size - indexOfRowP - 2));
    }

    private void prepareVectorQ() {
        this.vectorQ = new ArrayList<>();
        vectorQ.addAll(generateRandomVector(indexOfRowQ - 1));

        vectorQ.add(vectorA.get(indexOfRowQ));
        vectorQ.add(vectorC.get(indexOfRowQ));
        vectorQ.add(vectorB.get(indexOfRowQ));

        vectorQ.addAll(generateRandomVector(size - indexOfRowQ - 2));
    }

    private void prepareVectorF() {
        vectorF = new ArrayList<>();
        vectorF.addAll(generateRandomVector(size));
    }

    private void prepareVectorOfOnes() {
        vectorOfOnes = new ArrayList<>();
        vectorOfOnes.addAll(generateOneFilledVector(size));
    }

    private void printVector(List<Double> vector, String header) {
        System.out.println(header + " " + vector);
    }

    // ну это пока довольно топорно, но этого будет достаточно, для отладки алгоритма, над этим однозначно надо еще подумать
    public void printMatrix() {
        for (int i = 0; i < size; i++) {
            MatrixRow mr = new MatrixRow(size, i + 1);
            // в конструкторе есть такой параметр как номер строки(i+1) он планировался для помещения строк
            // в единую матрицу, на самом деле он ни на что не влияет
            if (i == 0) {
                mr.add(vectorC.get(i), i + 1);
                mr.add(vectorB.get(i), i + 2);
            } else if (i == size - 1) {
                mr.add(vectorA.get(i), i);
                mr.add(vectorC.get(i), i + 1);
            } else if (i == indexOfRowP) {
                mr.fillFromArray(vectorP);
            } else if (i == indexOfRowQ) {
                mr.fillFromArray(vectorQ);
            } else {
                mr.add(vectorA.get(i), i);
                mr.add(vectorC.get(i), i + 1);
                mr.add(vectorB.get(i), i + 2);
            }
            mr.print();


        }
        System.out.println("f: " + vectorF);
        System.out.println();
    }

    private void printVectors() {
        printVector(vectorA, "[vector A]:");
        printVector(vectorB, "[vector B]:");
        printVector(vectorC, "[vector C]:");
        printVector(vectorP, "[vector P]:");
        printVector(vectorQ, "[vector Q]:");
        printVector(vectorF, "[vector F]:");
        printVector(vectorOfOnes, "[vector Ones]:");
    }

    // Ужасно грустно с повторного кода, но пока без оптимизаций
    private void probegUpToDown(int from, int to, DirtyVectors mode) {
        if (from == 1) {
            if (mode == DirtyVectors.vectorQ || mode == DirtyVectors.both) {
                int index = 0;

                // для вектора Q
                double coeff = vectorQ.get(index) / vectorC.get(index);

                if (isDebug) {
                    System.out.print("UpToDown Q: ");
                    logOperation(index, indexOfRowQ, -coeff);
                }

                // q[index]
                double changedElement = vectorQ.get(index) - vectorC.get(index) * coeff;
                vectorQ.set(index, changedElement);

                // q[index + 1]
                changedElement = vectorQ.get(index + 1) - vectorB.get(index) * coeff;
                vectorQ.set(index + 1, changedElement);

                changedElement = vectorF.get(indexOfRowQ) - vectorF.get(index) * coeff;
                vectorF.set(indexOfRowQ, changedElement);

                changedElement = vectorOfOnes.get(indexOfRowQ) - vectorOfOnes.get(index) * coeff;
                vectorOfOnes.set(indexOfRowQ, changedElement);
            }
        }
        synchronizeVectors(indexOfRowP, DirtyVectors.vectorP);
        synchronizeVectors(indexOfRowQ, DirtyVectors.vectorQ);
        if (isDebug) {
            printMatrix();
        }

        for (int index = from; index < to; index++) {
            if (mode == DirtyVectors.vectorP || mode == DirtyVectors.both) {
                // для вектора P
                double coeff = vectorP.get(index - 1) / vectorA.get(index);

                if (isDebug) {
                    System.out.print("UpToDown P: ");
                    logOperation(index, indexOfRowP, -coeff);
                }

                // p[index - 1]
                double changedElement = vectorP.get(index - 1) - vectorA.get(index) * coeff;
                vectorP.set(index - 1, changedElement);

                // p[index]
                changedElement = vectorP.get(index) - vectorC.get(index) * coeff;
                vectorP.set(index, changedElement);

                // p[index + 1]
                changedElement = vectorP.get(index + 1) - vectorB.get(index) * coeff;
                vectorP.set(index + 1, changedElement);

                changedElement = vectorF.get(indexOfRowP) - vectorF.get(index) * coeff;
                vectorF.set(indexOfRowP, changedElement);

                changedElement = vectorOfOnes.get(indexOfRowP) - vectorOfOnes.get(index) * coeff;
                vectorOfOnes.set(indexOfRowP, changedElement);
            }

            if (mode == DirtyVectors.vectorQ || mode == DirtyVectors.both) {
                // для вектора Q
                double coeff = vectorQ.get(index - 1) / vectorA.get(index);

                if (isDebug) {
                    System.out.print("UpToDown Q: ");
                    logOperation(index, indexOfRowQ, -coeff);
                }

                // q[index - 1]
                double changedElement = vectorQ.get(index - 1) - vectorA.get(index) * coeff;
                vectorQ.set(index - 1, changedElement);

                // q[index]
                changedElement = vectorQ.get(index) - vectorC.get(index) * coeff;
                vectorQ.set(index, changedElement);

                // q[index + 1]
                changedElement = vectorQ.get(index + 1) - vectorB.get(index) * coeff;
                vectorQ.set(index + 1, changedElement);

                changedElement = vectorF.get(indexOfRowQ) - vectorF.get(index) * coeff;
                vectorF.set(indexOfRowQ, changedElement);

                changedElement = vectorOfOnes.get(indexOfRowQ) - vectorOfOnes.get(index) * coeff;
                vectorOfOnes.set(indexOfRowQ, changedElement);
            }
            if (isDebug) {
                printMatrix();
            }
        }
    }

    private void probegDownToUp(int from, int to, DirtyVectors mode) {
        for (int index = from; index > to; index--) {
            if (mode == DirtyVectors.vectorQ || mode == DirtyVectors.both) {
                // для вектора q
                double coeff = vectorQ.get(index + 1) / vectorB.get(index);
                if (isDebug) {
                    System.out.print("DownToUp P: ");
                    logOperation(index, indexOfRowQ, -coeff);
                }

                // q[index - 2]
                double changedElement = vectorQ.get(index - 1) - vectorA.get(index) * coeff;
                vectorQ.set(index - 1, changedElement);

                // q[index]
                changedElement = vectorQ.get(index) - vectorC.get(index) * coeff;
                vectorQ.set(index, changedElement);

                // q[index + 1]
                changedElement = vectorQ.get(index + 1) - vectorB.get(index) * coeff;
                vectorQ.set(index + 1, changedElement);

                changedElement = vectorF.get(indexOfRowQ) - vectorF.get(index) * coeff;
                vectorF.set(indexOfRowQ, changedElement);

                changedElement = vectorOfOnes.get(indexOfRowQ) - vectorOfOnes.get(index) * coeff;
                vectorOfOnes.set(indexOfRowQ, changedElement);
            }

            if (mode == DirtyVectors.vectorP || mode == DirtyVectors.both) {
                // для вектора p
                double coeff = vectorP.get(index + 1) / vectorB.get(index);
                if (isDebug) {
                    System.out.print("DownToUp Q: ");
                    logOperation(index, indexOfRowP, -coeff);
                }

                // q[index - 2]
                double changedElement = vectorP.get(index - 1) - vectorA.get(index) * coeff;
                vectorP.set(index - 1, changedElement);

                // q[index]
                changedElement = vectorP.get(index) - vectorC.get(index) * coeff;
                vectorP.set(index, changedElement);

                // q[index + 1]
                changedElement = vectorP.get(index + 1) - vectorB.get(index) * coeff;
                vectorP.set(index + 1, changedElement);

                changedElement = vectorF.get(indexOfRowP) - vectorF.get(index) * coeff;
                vectorF.set(indexOfRowP, changedElement);

                changedElement = vectorOfOnes.get(indexOfRowP) - vectorOfOnes.get(index) * coeff;
                vectorOfOnes.set(indexOfRowP, changedElement);
            }
            if (isDebug) {
                printMatrix();
            }
        }
    }

    public void normalizeMatrix() {
        probegUpToDown(1, indexOfRowP, DirtyVectors.both);
        synchronizeVectors(indexOfRowP, DirtyVectors.vectorP);

        probegUpToDown(indexOfRowP, indexOfRowQ, DirtyVectors.vectorQ);
        synchronizeVectors(indexOfRowQ, DirtyVectors.vectorQ);

        probegDownToUp(size - 2, indexOfRowQ, DirtyVectors.both);
        synchronizeVectors(indexOfRowQ, DirtyVectors.vectorQ);

        probegDownToUp(indexOfRowQ, indexOfRowP, DirtyVectors.vectorP);
        synchronizeVectors(indexOfRowP, DirtyVectors.vectorP);
    }

    private void synchronizeVectors(int atPosition, DirtyVectors with) {
        // нужно для синхронизации векторов A,C,B с вектором P или Q
        // в случае, когда достигнуто их пересечение
        if (with == DirtyVectors.vectorP) {
            vectorA.set(atPosition, vectorP.get(atPosition - 1));
            vectorC.set(atPosition, vectorP.get(atPosition));
            vectorB.set(atPosition, vectorP.get(atPosition + 1));
        } else if (with == DirtyVectors.vectorQ) {
            vectorA.set(atPosition, vectorQ.get(atPosition - 1));
            vectorC.set(atPosition, vectorQ.get(atPosition));
            vectorB.set(atPosition, vectorQ.get(atPosition + 1));
        }
    }

    private void line() {
        System.out.println("---------------------------");
    }

    enum DirtyVectors {
        vectorP,
        vectorQ,
        both
    }

    public void solveMatrix() {
        if (isDebug) {
            line();
            System.out.println("Before tripple run");
            printMatrix();
            line();
        }
        vectorA.remove(0);
        vectorB.remove(vectorB.size() - 1);
        solveVector = TripleRunner.run(vectorA, vectorB, vectorC, vectorF);
        solveVectorInOnes = TripleRunner.run(vectorA, vectorB, vectorC, vectorOfOnes);
        vectorA.add(0, 0.0);
        vectorB.add(0.0);
        if (isDebug) {
            System.out.println("After tripple run");
            printMatrix();
            line();
        }
    }

    public List<Double> getSolutionInRandom() {
        normalizeMatrix();
        solveMatrix();
        return solveVector;
    }

    public List<Double> getSolutionInOnes() {
        if (solveVectorInOnes == null || solveVectorInOnes.size() == 0) {
            normalizeMatrix();
            solveMatrix();
        }
        return solveVectorInOnes;
    }

    public void prepareFake() {
        size = 5;
        vectorA = new ArrayList<>();
        vectorB = new ArrayList<>();
        vectorC = new ArrayList<>();
        vectorF = new ArrayList<>();
        vectorP = new ArrayList<>();
        vectorQ = new ArrayList<>();
        prepareVectorOfOnes();


        vectorA.add(0.0);
        vectorA.add(-3.0);
        vectorA.add(-5.0);
        vectorA.add(-6.0);
        vectorA.add(-5.0);

        vectorB.add(-1.0);
        vectorB.add(-1.0);
        vectorB.add(2.0);
        vectorB.add(-4.0);
        vectorB.add(0.0);

        vectorC.add(2.0);
        vectorC.add(8.0);
        vectorC.add(12.0);
        vectorC.add(18.0);
        vectorC.add(10.0);

        vectorP.add(-3.0);
        vectorP.add(8.0);
        vectorP.add(-1.0);
        vectorP.add(-5.0);
        vectorP.add(10.0);

        vectorQ.add(2.0);
        vectorQ.add(-1.0);
        vectorQ.add(-6.0);
        vectorQ.add(18.0);
        vectorQ.add(-4.0);

        vectorF.add(-25.0);
        vectorF.add(92.0);
        vectorF.add(-69.0);
        vectorF.add(-(156.0 + 25));
        vectorF.add(20.0);
    }

    private void logOperation(int from, int to, double coeff) {
        System.out.println(from + " --> " + to + " with " + coeff);
    }
}
