package main.lab.third.justTry.MatrixSolution;

import Errors.OutOfBounds;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Anton
 *
 */
public class MatrixRow {

    private List<Double> value;
    private List<Integer> indices;//starts with 1
    private int rowNum;
    private int size;

    public MatrixRow(int size, int row) {
        value = new ArrayList<>(size);
        indices = new ArrayList<>(size);
        rowNum = row;
        this.size = size;
    }
    public MatrixRow(double[] arr, int row) {
        value = new ArrayList<>(size);
        indices = new ArrayList<>(size);
        rowNum = row;
        size = arr.length;
        fillFromArray(arr);
    }

    public void add(double v, int i){
        if(i < 1 && i > size)
            throw new OutOfBounds();//индексы, как в обычной строке матрицы
        //неважно на каком месте в списке находятся значения и индексы, важно, что они имеют одинаковые
        //индексы в листе.
        if(v == 0)
            return;
        value.add(v);
        indices.add(i);
    }
    public double getElement(int i){
        if(i < 1 && i > size)
            throw new OutOfBounds();//индексы, как в обычной строке матрицы
        int index = indices.indexOf(i);
        if(index == -1)
            return 0;
        return value.get(index);
    }
    public void mul(double x) {
        value = value.stream()
                .map(v -> v * x)
                .collect(Collectors.toList());
    }
    public void addRow(MatrixRow mr) {
        for (int index: mr.indices) {
            if(getElement(index) == 0)//если этот элемент 0, то для строки он новый
                add(mr.getElement(index), index);
            else {
                int i = indices.indexOf(index);
                value.set(i, value.get(i) + mr.getElement(index));
            }
        }
    }





    public int size(){
        return size;
    }
    public void print() {
        for(int i = 0 ; i < size ; i ++) {
            System.out.print(String.format("%15g", getElement(i + 1)));
        }
        System.out.println();
    }
    public void randomFill() {
        for(int i = 0 ; i < size ; i ++) {
            double value = (int) (Math.random() * 3) + 1;
            add(value, i + 1);
        }
    }
    public void fillFromArray(List<Double> arr) {
        if(!value.isEmpty())
            return;
        for(int i = 0 ; i < arr.size() ; i ++) {
            double x = arr.get(i);
            if(arr.get(i) == 0)
                continue;
            int index = i + 1;
            add(x, index);
        }
    }
    public void fillFromArray(double[] arr) {
        if(!value.isEmpty())
            return;
        for(int i = 0 ; i < arr.length ; i ++) {
            double x = arr[i];
            if(arr[i] == 0)
                continue;
            int index = i + 1;
            add(x, index);
        }
    }
}
