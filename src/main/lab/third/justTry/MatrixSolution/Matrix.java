package main.lab.third.justTry.MatrixSolution;

import Errors.OutOfBounds;

import java.util.ArrayList;
import java.util.List;

public class Matrix {

    private List<MatrixRow> matrixRowList;
    private int rows;
    private int cols;

    enum Operations {
        MUL,
        ADDROW
    }

    public Matrix(List<MatrixRow> list) {
        matrixRowList = list;
        rows = list.size();
        cols = list.get(0).size();
    }
    public Matrix(double[][] array) {
        matrixRowList = new ArrayList<>();

        int i = 1;// строки нумеруются с 1.
        for (var arr: array) {
            MatrixRow a = new MatrixRow(arr, i);
            matrixRowList.add(a);
            i+= 1;
        }
        rows = array.length;
        cols = array[0].length;
    }


    private MatrixRow getRow(int i) {
        if(i < 1 || i > rows)
            throw new OutOfBounds();
        return matrixRowList.get(i - 1);
    }
    private void checkRowBounds(int index) {
        if(index < 1 || index > rows)
            throw new OutOfBounds();
    }
    private void checkColsBounds(int index) {
        if(index < 1 || index > cols)
            throw new OutOfBounds();
    }
    private void logMul(int index, double coef) {
//        System.out.println("Row " + index + " * " + coef);
    }
    private void logAdd(int from, int to) {
//        System.out.println("Row " + to + " += Row " + from);
    }


    public void mulRow(int i, double coefficient) {
        checkRowBounds(i);

        var row = getRow(i);
        row.mul(coefficient);

        logMul(i, coefficient);
    }
    public void addRow(int from, int to) {
        checkRowBounds(from);
        checkRowBounds(to);


        var destintaion = getRow(to);
        destintaion.addRow(getRow(from));

        logAdd(from, to);
    }
    public void addRow(int from, int to, double coefficient){
        checkRowBounds(from);
        checkRowBounds(to);

        var destintaion = getRow(to);
        mulRow(from, coefficient);
        destintaion.addRow(getRow(from));
        logAdd(from,to);
        mulRow(from, 1/coefficient);

    }
    public double get(int i, int j) {
        checkRowBounds(i);
        checkColsBounds(j);

        return getRow(i).getElement(j);
    }







    public void printMatrix() {
//        for (var row: matrixRowList) {
//            row.print();
//        }
//        System.out.println();
    }
    public double[][] getMatrixAsArray() {
        double[][] arrayMatrix = new double[rows][cols];

        for(int i = 1 ; i <= rows ; i ++) {
            for(int j = 1 ; j <= cols ; j ++) {
                arrayMatrix[i - 1][j - 1] = getRow(i).getElement(j);
            }
        }

        return arrayMatrix;
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }
}
