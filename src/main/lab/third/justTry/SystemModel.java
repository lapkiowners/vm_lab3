package main.lab.third.justTry;

import Errors.ImpossibleToPutDirtyVectorException;
import Errors.TinyMatrixException;
import main.lab.third.justTry.util.dataparser.DataParser;
import main.lab.third.justTry.util.random.RandomDoubleGenerator;
import main.lab.third.justTry.util.random.RandomGenerator;
import main.lab.third.justTry.util.validation.InputValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, представляющий собой контейнер для решаемой СЛАУ.
 *
 */
public class SystemModel {

    private final int size;//размер
    private final int indexOfRowP;//индекс первой грязной строки
    private final int indexOfRowQ;//индекс второй грязной строки

    private final RandomGenerator generator;//генератор случайных чисел

    //Вектора:

    //Диагональные вектора
    private List<Double> vectorA;
    private List<Double> vectorB;
    private List<Double> vectorC;

    //Гразные вектора
    private List<Double> vectorP;
    private List<Double> vectorQ;

    //Вектор свободных членов
    private List<Double> vectorF;
    private List<Double> vectorFforOnes;


    //Модель СЛАУ задается 4 параметрами: генератором, с заданными границами генерации, размером и двумя индексами грязных строк
    public SystemModel(RandomGenerator random, int size, int indexOfRowP, int indexOfRowQ) {
        //при инициализации проводится проверка на возможность существования такой СЛАУ
        checkStartingValidity(size, indexOfRowP);

        generator = random;
        this.size = size;
        this.indexOfRowP = indexOfRowP;
        this.indexOfRowQ = indexOfRowQ;

        //Если все ОК, то генерируем вектора образующие матрицу, БЕЗ СВОБОДНЫХ ЧЛЕНОВ.
        prepareVectors();
    }
    //2 довольно глупых конструктора
    public SystemModel(RandomGenerator random, int size, int indexK) {
        this(random,size,indexK,indexK + 2);
    }
    public SystemModel(RandomGenerator random, String filename) {
        this(random, DataParser.InputFromFile(filename).getSize(),
                DataParser.InputFromFile(filename).getIndexOfRowP());
    }

    private void checkStartingValidity(int size, int indexK) {
        InputValidator validator = new InputValidator();

        if(!validator.isValid( (x) -> x > 4, size)){
            throw new TinyMatrixException();
        }
        if(!validator.isValid( (x) -> x > (indexK + 2), size)){
            throw new ImpossibleToPutDirtyVectorException("Q");
        }
        if(!validator.isValid( (x) -> x >= indexK, size)){
            throw new ImpossibleToPutDirtyVectorException("P");
        }
    }

    //Обертки для методов генерации
    private List<Double> generateRandomVector(int size) {
        return generator.getRandomList(size);
    }
    private List<Double> generateOneFilledVector(int size) {
        return generator.getFilledOneList(size);
    }
    private List<Double> generateZeroFilledVector(int size) {
        return generator.getFilledZeroList(size);
    }

    //Обертка для генерации векторов
    private void prepareVectors() {
        prepareVectorA();
        prepareVectorB();
        prepareVectorC();
        prepareVectorP();
        prepareVectorQ();
    }

    //Генерация векторов
    private void prepareVectorA() {
        vectorA = new ArrayList<>();
        vectorA.add(0.0);           // в векторе А нет нулевого элемента, добавлено искусственно ради нормальной нумерации
        vectorA.addAll(generateRandomVector(size - 1));
    }
    private void prepareVectorB() {
        vectorB = new ArrayList<>();
        vectorB.addAll(generateRandomVector(size - 1));
        vectorB.add(0.0);           // в векторе В нет n-ого элемента, добавлено искусственно ради нормальной нумерации
    }
    private void prepareVectorC() {
        vectorC = new ArrayList<>();
        vectorC.addAll(generateRandomVector(size));
    }
    private void prepareVectorP() {
        vectorP = new ArrayList<>();
        vectorP.addAll(generateRandomVector(indexOfRowP - 1));

        vectorP.add(vectorA.get(indexOfRowP));
        vectorP.add(vectorC.get(indexOfRowP));
        vectorP.add(vectorB.get(indexOfRowP));

        this.vectorP.addAll(generateRandomVector(size - indexOfRowP - 2));
//        this.vectorP.addAll(generateZeroFilledVector(size));
    }
    private void prepareVectorQ() {
        this.vectorQ = new ArrayList<>();
        vectorQ.addAll(generateRandomVector(indexOfRowQ - 1));

        vectorQ.add(vectorA.get(indexOfRowQ));
        vectorQ.add(vectorC.get(indexOfRowQ));
        vectorQ.add(vectorB.get(indexOfRowQ));

        vectorQ.addAll(generateRandomVector(size - indexOfRowQ - 2));
//        this.vectorQ.addAll(generateZeroFilledVector(size));
    }

    //Вектор свободных членов в модели является опциональным и
    // генерируется при необходимости с помощью обращения к методу из вызывающего кода
    public void prepareVectorF() {
        vectorF = new ArrayList<>();
        vectorF.addAll(generateOneFilledVector(size));
    }

    public int getSize() {
        return size;
    }
    public int getIndexOfRowP() {
        return indexOfRowP;
    }
    public int getIndexOfRowQ() {
        return indexOfRowQ;
    }
    public List<Double> getVectorA() {
        return vectorA;
    }
    public List<Double> getVectorB() {
        return vectorB;
    }
    public List<Double> getVectorC() {
        return vectorC;
    }
    public List<Double> getVectorF() {
        return vectorF;
    }
    public List<Double> getVectorFOfOnes() {
        return vectorFforOnes;
    }
    public List<Double> getVectorP() {
        return vectorP;
    }
    public List<Double> getVectorQ() {
        return vectorQ;
    }

    //В силу опциональности добавлена возможность установки своего вектора свободных членов
    public void setVectorF(List<Double> vectorF) {
        this.vectorF = vectorF;
    }

    public void setVectorsF(List<Double> vectorF, List<Double> vectorFforOnes) {
        this.vectorF = vectorF;
        this.vectorFforOnes = vectorFforOnes;
    }

    private double getElementOfMatrix(int i, int j) {
        if (i == indexOfRowP) {
            return vectorP.get(j);
        }
        if (i == indexOfRowQ) {
            return vectorQ.get(j);
        }
        if (i == j) {
            return vectorC.get(j);
        }
        if (i - 1 == j) {
            return vectorA.get(i);
        }
        if (j - 1 == i) {
            return vectorB.get(i);
        }
        return 0.0;
    }

    //Конвертация в матрицу БЕЗ ВЕКТОРА СВОБОДНЫХ ЧЛЕНОВ.
    public double[][] getMatrix() {
        double[][] matrix = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = getElementOfMatrix(i, j);
            }
        }
        return matrix;
    }
    public double[][] getMatrixWithF() {
        double[][] matrix = new double[size][size + 1];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = getElementOfMatrix(i, j);

            }
            matrix[i][size] = vectorF.get(i);
        }
        return matrix;
    }
    public double[][] getMatrixWithFs() {
        double[][] matrix = new double[size][size + 2];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = getElementOfMatrix(i, j);

            }
            matrix[i][size] = vectorFforOnes.get(i);
            matrix[i][size + 1] = vectorF.get(i);
        }
        return matrix;
    }
}
