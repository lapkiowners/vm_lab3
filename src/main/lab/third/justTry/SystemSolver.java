package main.lab.third.justTry;

import main.lab.third.justTry.MatrixSolution.Matrix;
import main.lab.third.justTry.MatrixSolution.MatrixRow;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, предоставляющий алгоритмическое решение для СЛАУ, представленной SystemModel
 */
public class SystemSolver {
    //одноименные параметры, аналогичны для параметров в SystemModel
    private int size;
    private int indexOfRowP;
    private int indexOfRowQ;                //хотя оно и будет вычисляться, как indexOfRowP + 2

    private int matrixP;
    private int matrixQ;

    private List<Double> vectorA;
    private List<Double> vectorB;
    private List<Double> vectorC;

    private List<Double> vectorP;
    private List<Double> vectorQ;

    private List<Double> vectorF;
    private List<Double> vectorFofOnes;

    private Matrix matrix;

    //Новый вектор, представляющий из себя решение СЛАУ
    private List<Double> solveVector;
    private List<Double> solveVectorForOnes;

    //Режим для запуска алгоритма
    private boolean isDebug = false;

    private final int k = 2;

    enum DirtyVectors {
        vectorP,
        vectorQ,
        both
    }

    enum mode {
        upToDown,
        downToUp
    }

    //Были убраны 2 конструктора:с параметрами размера  и ввводом из файла, они оба перенесены в SystemModel
    public SystemSolver(SystemModel model) {

        this.size = model.getSize();
        this.indexOfRowP = model.getIndexOfRowP();
        this.indexOfRowQ = model.getIndexOfRowQ();

        getVectorsFromModel(model);
        matrix = new Matrix(model.getMatrixWithFs());

        matrixP = indexOfRowP + 1;
        matrixQ = matrixP + k;
    }

    //КОНФИГУРАЦИОННЫЕ МЕТОДЫ

    private void getVectorsFromModel(SystemModel model) {
        vectorA = model.getVectorA();
        vectorB = model.getVectorB();
        vectorC = model.getVectorC();
        vectorF = model.getVectorF();
        vectorFofOnes = model.getVectorFOfOnes();
        vectorP = model.getVectorP();
        vectorQ = model.getVectorQ();
    }

    //ГЛАВНЫЙ МЕТОД КЛАССА

    public List<Double> getSolution() {
        normalizeMatrix();
        solveMatrix();
        return solveVector;
    }

    public List<Double> getSolutionForOnes() {
        return solveVectorForOnes;
    }


    //АЛГОРИТМИЧЕСКИЕ МЕТОДЫ

    //Тут я просто попытался выделить общие куски кода в проходах сверху вниз и снизу вверх



    public void upToDown() {
        for(int i = 1 ; i < matrixP ; i ++) {
            double coefficient = 1 / matrix.get(i, i);

            matrix.mulRow(i, coefficient);


            matrix.addRow(i, matrixP, -matrix.get(matrixP, i));
            matrix.addRow(i, matrixQ, -matrix.get(matrixQ, i));
            if(i + 1 != matrixP)
                matrix.addRow(i, i + 1, -matrix.get(i + 1, i));
            matrix.printMatrix();
        }
    }
    private void downToUp() {
        for(int i = matrix.getRows() ; i > matrixQ ; i --) {
            double coefficient = 1 / matrix.get(i, i);

            matrix.mulRow(i, coefficient);


            matrix.addRow(i, matrixP, -matrix.get(matrixP, i));
            matrix.addRow(i, matrixQ, -matrix.get(matrixQ, i));
            if(i - 1 != matrixQ)
                matrix.addRow(i, i - 1, -matrix.get(i - 1, i));
            matrix.printMatrix();
        }
    }

    private void cleanRect() {
        //ну это пока говнокод ок да
        matrix.mulRow(matrixP, 1/matrix.get(matrixP,matrixP));
        matrix.addRow(matrixP, matrixP + 1, -matrix.get(matrixP + 1, matrixP));
        matrix.addRow(matrixP, matrixP + 2, -matrix.get(matrixP + 2, matrixP));

        matrix.printMatrix();

        matrix.mulRow(matrixP + 1, 1/matrix.get(matrixP + 1,matrixP + 1));
        matrix.addRow(matrixP + 1, matrixP, -matrix.get(matrixP, matrixP + 1));
        matrix.addRow(matrixP + 1, matrixQ, -matrix.get(matrixQ, matrixP + 1));

        matrix.printMatrix();

        matrix.mulRow(matrixQ, 1/matrix.get(matrixQ,matrixQ));
        matrix.addRow(matrixQ, matrixP, -matrix.get(matrixP, matrixQ));
        matrix.addRow(matrixQ, matrixP + 1, -matrix.get(matrixP + 1, matrixQ));

        matrix.printMatrix();
    }

    private void cleanOther() {
        for(int i = matrixP ; i > 1 ; i --) {
            double coefficient = -matrix.get(i - 1, i);

            matrix.addRow(i, i - 1, coefficient);

            matrix.printMatrix();
        }

        for(int i = matrixQ ; i < matrix.getRows(); i ++) {
            double coefficient = -matrix.get(i + 1, i);

            matrix.addRow(i, i + 1, coefficient);

            matrix.printMatrix();
        }
    }

    public void normalizeMatrix() {
        upToDown();
        downToUp();
        cleanRect();
        cleanOther();
    }




    public void solveMatrix() {
        printMatrix();
        solveVector = new ArrayList<>();
        solveVectorForOnes = new ArrayList<>();
        for(int i = 1 ; i <= matrix.getRows() ; i ++) {
            solveVector.add(matrix.get(i, matrix.getCols()));
            solveVectorForOnes.add(matrix.get(i, matrix.getCols() - 1));
        }
        matrix.printMatrix();
    }


    //МЕТОДЫ ДЛЯ ЛОГИРОВАНИЯ

    private void logOperation(int from, int to, double coeff) {
        System.out.println(from + " --> " + to + " with " + coeff);
    }

    public void setIsDebug(boolean isDebug) {
        this.isDebug = isDebug;
    }

    private void line() {
        System.out.println("---------------------------");
    }

    private void printVector(List<Double> vector, String header) {
        System.out.println(header + " " + vector);
    }

    // ну это пока довольно топорно, но этого будет достаточно, для отладки алгоритма, над этим однозначно надо еще подумать
    public void printMatrix() {
        matrix.printMatrix();

    }

    private void printVectors() {
        printVector(vectorA, "[vector A]:");
        printVector(vectorB, "[vector B]:");
        printVector(vectorC, "[vector C]:");
        printVector(vectorP, "[vector P]:");
        printVector(vectorQ, "[vector Q]:");
        printVector(vectorF, "[vector F]:");
    }

}
