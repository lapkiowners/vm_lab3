package main.lab.third.justTry.util.output;

import main.lab.third.justTry.util.test.TestRunner;

public class OutputPrinter {

    private void header() {
        System.out.println("№ теста | размерность системы | диапазон значений | ср. относит. погрешность|" +
                            " ср. относит.оценки точности | кол-во тестов для усреднения " );
    }

    private void line() {
        System.out.println("-----------------------------------------------------------------------" +
                "-----------------------------------------------------------------------------");
    }

    public void print() {
        header();
        for(int i = 0 ; i < 3 ; i ++) {
            int size = (int) Math.pow(10, i + 1);
            for (int j = 0 ; j < 3 ; j ++) {
                int bot = (int) -(Math.pow(10, j + 1));
                int top = -bot;

                TestRunner runner = new TestRunner(size, 3, 5);
                runner.setGeneratorParams(bot, top);
                double averagePrecision = runner.calculateAveragePrecision();
                double averageAccuracy = runner.calculateAverageAccuracy();
                printLine(i * 3 + j + 1, size, top, averagePrecision, averageAccuracy, runner.getTimes());
                line();
            }
        }
    }

    public void printLine(int num, int size, int diff, double average, double averageRating, int count) {
        System.out.print(String.format("%8d|", num));
        System.out.print(String.format("%21d|", size));
        System.out.print(String.format("%19d|", diff));
        System.out.print(String.format("%26g|", average));
        System.out.print(String.format("%28g|", averageRating));
        System.out.println(String.format("%28d|", count));
    }

    public static void main(String[] args) {
        OutputPrinter printer = new OutputPrinter();
        printer.print();
    }
}
