package main.lab.third.justTry.util.random;

import java.util.List;

public interface RandomGenerator<Type> {

    public List<Type> getFilledOneList(int size);
    public List<Type> getFilledZeroList(int size);
    public List<Type> getRandomList(int size);
    public Type getRandomValue();
}
