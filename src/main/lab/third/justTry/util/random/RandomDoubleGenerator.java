package main.lab.third.justTry.util.random;

import com.sun.java.accessibility.util.TopLevelWindowListener;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class RandomDoubleGenerator implements RandomGenerator<Double> {

    private Random random = new Random(System.currentTimeMillis());
    private final double TOP;
    private final double BOTTOM;

    public RandomDoubleGenerator(double BOTTOM, double TOP) {
        if(BOTTOM >= TOP)
            throw new IllegalArgumentException("BOTTOM must be greater than TOP");
        this.TOP = TOP;
        this.BOTTOM = BOTTOM;
    }
    public RandomDoubleGenerator() {
        this(10, 0);
    }

    @Override
    public List<Double> getRandomList(int size) {

        List<Double> list = Stream
                .generate(this::getRandomValue)
                .limit(size)
                .collect(Collectors.toList());

        return list;
    }

    @Override
    public Double getRandomValue() {
        Double value = (Math.random() * (TOP - BOTTOM)) + BOTTOM;
        while (value == 0) {
            value = getRandomValue();
        }

        if(value > TOP || value < BOTTOM)
            throw new IllegalArgumentException();
        return value;
    }

    @Override
    public List<Double> getFilledOneList(int size) {
        List<Double> list = Stream
                .generate(() -> 1d)
                .limit(size)
                .collect(Collectors.toList());

        return list;
    }

    @Override
    public List<Double> getFilledZeroList(int size) {
        List<Double> list = Stream
                .generate(() -> 0d)
                .limit(size)
                .collect(Collectors.toList());

        return list;
    }
}
