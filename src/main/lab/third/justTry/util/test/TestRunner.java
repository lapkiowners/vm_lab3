package main.lab.third.justTry.util.test;

import main.lab.third.justTry.util.random.RandomDoubleGenerator;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.NaN;

public class TestRunner {

    private final int EFFORT_TIMES;

    private SystemTest effort;
    private RandomDoubleGenerator generator;

    private int size;
    private int indexOfRowP;
    private int indexOfRowQ;

    private double bot;
    private double top;

    private List<Double> deltasOfPrecision;
    private List<Double> deltasOfAccuracy;
    private int times;

    private Double accuracy;
    private Double precision;

    public TestRunner(int size, int indexOfRowP, int indexOfRowQ) {
        this(10, size, indexOfRowP, indexOfRowQ);
    }

    public TestRunner(int count, int size, int indexOfRowP, int indexOfRowQ) {
        EFFORT_TIMES = count;
        times = count;

        this.size = size;
        this.indexOfRowP = indexOfRowP;
        this.indexOfRowQ = indexOfRowQ;

        top = 1;
        bot = 0;
    }

    public void setGeneratorParams(double bot, double top) {
        this.bot = bot;
        this.top = top;
        makeGenerator();
    }

    private void makeGenerator() {
        generator = new RandomDoubleGenerator(bot, top);
    }

    // Производит вычисления 1 теста
    private void goOneTest() {
        effort = new SystemTest(generator, size, indexOfRowP, indexOfRowQ);
        deltasOfPrecision.add(effort.getPrecision());
        deltasOfAccuracy.add(effort.getAccuracy());

    }

    private void run() {
        if (generator == null) {
            makeGenerator();
        }

        deltasOfPrecision = new ArrayList<>();
        deltasOfAccuracy = new ArrayList<>();

        for (int i = 0; i < EFFORT_TIMES; i++) {
            goOneTest();
        }
        calculatePrecision();
        calculateAccuracy();
    }

    private void calculatePrecision() {
        double sum = 0;

        for (Double precision: deltasOfPrecision) {
            if (Double.isNaN(precision)) {
                times -= 1;
            } else {
                sum += precision;
            }
        }

        this.precision = sum / times;
    }

    private void calculateAccuracy() {
        double sum = 0;
        times = EFFORT_TIMES;

        for (Double accuracy: deltasOfAccuracy) {
            if (Double.isNaN(accuracy)) {
                times -= 1;
            } else {
                sum += accuracy;
            }
        }

        this.accuracy = sum / times;
    }

    public int getTimes() {
        return times;
    }

    public double calculateAveragePrecision() {
        if (precision == null) {
            run();
        }
        return precision;
    }

    public double calculateAverageAccuracy() {
        if (accuracy == null) {
            run();
        }
        return accuracy;
    }

}
