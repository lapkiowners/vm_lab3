package main.lab.third.justTry.util.test;

import main.lab.third.justTry.SystemModel;
import main.lab.third.justTry.SystemSolver;
import main.lab.third.justTry.util.random.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, представляющий собой тест-контейнер одного конкретного случая
 *
 */
public class SystemTest {

    private SystemModel model;
    private final RandomGenerator generator;

    private double[][] matrix;

    private List<Double> vectorA;
    private List<Double> vectorB;
    private List<Double> vectorC;
    private List<Double> vectorP;
    private List<Double> vectorQ;

    private List<Double> randomSolution;
    private List<Double> onesSolution;
    private List<Double> vectorFofRandomSolution;
    private List<Double> vectorFofOnesSolution;
    private List<Double> algorithmicSolve;
    private List<Double> algorithmicSolveOfOnes;
    private Double precision;
    private Double accuracy;

    private double q = 0.01;

        /**
         *
         * @param random - генератор с заданными условиями генерации чисел
         * @param size - размер матрицы
         * @param indexOfRowP - индекс первой грязной строки
         * @param indexOfRowQ - индекс второй грязной строки
         */
    public SystemTest(RandomGenerator random, int size, int indexOfRowP, int indexOfRowQ) {
            //создаем модель, которая автоматически генерирует матрицу, а именно 5 векторов:3 по диагонали и 2 грязных
            model = new SystemModel(random, size, indexOfRowP, indexOfRowQ);

            generator = random;


    }

    //методы для подсчета относительной погрешности
    private void calculateFs() {
        vectorFofRandomSolution = new ArrayList<>();
        double x[] = randomSolution.stream()
                .mapToDouble(value -> value)
                .toArray();

        for(int i = 0 ; i < matrix.length ; i ++) {
            double f = 0;
            for(int j = 0 ; j < matrix[i].length ; j ++) {
                f += matrix[i][j] * x[j];
            }
            vectorFofRandomSolution.add(f);
        }

        vectorFofOnesSolution = new ArrayList<>();
        x = onesSolution.stream()
                .mapToDouble(value -> value)
                .toArray();

        for(int i = 0 ; i < matrix.length ; i ++) {
            double f = 0;
            for(int j = 0 ; j < matrix[i].length ; j ++) {
                f += matrix[i][j] * x[j];
            }
            vectorFofOnesSolution.add(f);
        }
    }
    private void calculateAlgorithmicSolve(SystemSolver systemSolver) {
        algorithmicSolve = systemSolver.getSolution();
    }
    private void calculateAlgorithmicSolveOfOnes(SystemSolver systemSolver) {
        algorithmicSolveOfOnes = systemSolver.getSolutionForOnes();
    }
    private void calculatePrecisions() {
        //получаем из модели двумерный массив, представляющий из себя матрицу
        matrix = model.getMatrix();

        //генерируем случайное решение
        randomSolution =  generator.getRandomList(model.getSize());
        onesSolution = generator.getFilledOneList(model.getSize());

        //подсчитываем вектор значений путем перемножения матрицы на вектор решения (матрцы Х решение)
        calculateFs();

        //помещаем в нашу модель получившийся вектор
        model.setVectorsF(vectorFofRandomSolution, vectorFofOnesSolution);

        //инициализируем алгоритм с помощью модели, при инициализации:стартует проверка на решабельность, вытягиваются индексы, а затем вектора
        SystemSolver effort = new SystemSolver(model);

        //считаем решение с помощью алгоритма
        calculateAlgorithmicSolve(effort);
        calculateAlgorithmicSolveOfOnes(effort);

        double precision = 0.0;
        double accuracy = 0.0;
        for (int i = 0; i < randomSolution.size(); i++) {
            double newAccuracy = Math.abs(randomSolution.get(i) - algorithmicSolve.get(i));
            if (Math.abs(algorithmicSolve.get(i)) > Math.abs(q)) {
                newAccuracy /= Math.abs(q);
            }
            accuracy = Math.max(newAccuracy, accuracy);
        }
        this.accuracy = accuracy;
        for (int i = 0; i < onesSolution.size(); i++) {
            precision = Math.max(Math.abs(onesSolution.get(i) - algorithmicSolveOfOnes.get(i)), precision);
        }
        this.precision = precision;
    }

    public Double getPrecision() {
        if (precision == null || accuracy == null) {
            calculatePrecisions();
        }
        return precision;
    }

    public Double getAccuracy() {
        if (precision == null || accuracy == null) {
            calculatePrecisions();
        }
        return accuracy;
    }

    //методы для подсчета оценки точности


}
