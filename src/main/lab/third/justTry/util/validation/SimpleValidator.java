package main.lab.third.justTry.util.validation;

import java.util.function.Predicate;

public interface SimpleValidator<Type> {

    public default boolean isValid(Predicate<Type> condition, Type value) {
       return condition.test(value);
    }
}
