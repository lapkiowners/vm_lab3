package main.lab.third.justTry.util.validation;

import java.util.function.Predicate;

public class InputValidator implements SimpleValidator<Integer>{

    @Override
    public boolean isValid(Predicate<Integer> condition, Integer value) {
        return condition.test(value);
    }
}
