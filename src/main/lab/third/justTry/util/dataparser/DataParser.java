package main.lab.third.justTry.util.dataparser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DataParser {
    //private final String path = "vm_lab3\\src\\main\\source\\";
    private static String defaultPath = "src\\main\\source\\";



    private DataParser(){}
    public static DataModel InputFromFile(String filename) {

        String path = defaultPath + filename;
        int size = 0;
        int indexOfRowP = 0;

        System.out.println(path);
        {
            Scanner scan;
            try {
                scan = new Scanner(new File(path)).useDelimiter("\n");
                int i = 0;
                while (scan.hasNext()) {
                    String s = scan.next();
                    if (i == 0) {
                        Scanner sc = new Scanner(s).useDelimiter(" ");
                        while (sc.hasNext()) {
                            size =(int) (Double.parseDouble(sc.next()));
                        }
                        i++;
                    } else if (i == 1) {
                        Scanner sc = new Scanner(s).useDelimiter(" ");
                        while (sc.hasNext()) {
                            indexOfRowP =(int) (Double.parseDouble(sc.next()));
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
                e.printStackTrace();
            }
        }
        return new DataModel(size, indexOfRowP);
    }
}
