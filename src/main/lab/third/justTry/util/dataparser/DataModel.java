package main.lab.third.justTry.util.dataparser;

public class DataModel {
    private int size;
    private int indexOfRowP;

    public DataModel(int size, int indexOfRowP) {
        this.size = size;
        this.indexOfRowP = indexOfRowP;
    }

    public int getSize() {
        return size;
    }

    public int getIndexOfRowP() {
        return indexOfRowP;
    }
}
