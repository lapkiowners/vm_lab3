package main.lab.third.justTry.util;

import main.lab.third.justTry.util.random.RandomDoubleGenerator;
import main.lab.third.justTry.util.random.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

public class TripleRunner {
    public static List<Double> run(List<Double> vectorA, List<Double> vectorB, List<Double> vectorC, List<Double> vectorF) {
//        System.out.println("In tripple run: ");
//        System.out.println("[vector A]:" + vectorA);
//        System.out.println("[vector B]:" + vectorB);
//        System.out.println("[vector C]:" + vectorC);
//        System.out.println("[vector F]:" + vectorF);

        int size = vectorC.size();

        List<Double> mu = new ArrayList<>(size - 1);
        List<Double> nu = new ArrayList<>(size - 1);

        mu.add(-vectorB.get(0) / vectorC.get(0));
        nu.add(vectorF.get(0) / vectorC.get(0));

        for (int i = 1; i < size - 1; i++) {
            double Di = vectorC.get(i) + vectorA.get(i - 1) * mu.get(i - 1);
            mu.add((-vectorB.get(i) / Di));
            nu.add((vectorF.get(i) - vectorA.get(i - 1) * nu.get(i - 1)) / Di);
        }

        double[] c = new double[size];
        c[size - 1] = (vectorF.get(size - 1) - vectorA.get(size - 2) * nu.get(size - 2))
                / (vectorA.get(size - 2) * mu.get(size - 2) + vectorC.get(size - 1));
        for (int i = size - 2; i >= 0; i--) {
            c[i] = mu.get(i) * c[i + 1] + nu.get(i);
        }

        List<Double> solveVector = new ArrayList<>();
        for (double aC : c) {
            solveVector.add(aC);
        }
        return solveVector;
    }

    public static void main(String[] args) {
        RandomGenerator generator = new RandomDoubleGenerator(0, 10);
        List<Double> vectorA = generator.getRandomList(10 - 1);
        List<Double> vectorB = generator.getRandomList(10 - 1);
        List<Double> vectorC = generator.getRandomList(10);
        List<Double> vectorF = generator.getRandomList(10);

        System.out.println("A:" + vectorA);

        System.out.println("C:" + vectorC);
        System.out.println("B:" + vectorB);
        System.out.println("F:" + vectorF);


        var solve = TripleRunner.run(vectorA, vectorB, vectorC, vectorF);

        System.out.println("solve:" + solve);
    }

}
