package Errors;

public class TinyMatrixException extends MatrixException {
    // в матрице как минимум должно быть 3 строки, а по хорошему - 4
    @Override
    public String getMessage() {
        return "Размер создаваемой матрицы недостаточно велик!";
    }
}
