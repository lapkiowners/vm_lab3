package Errors;

public class ZeroAtDiagonalException extends MatrixException {

    @Override
    public String getMessage() {
        return "Диагональ содержит 0!";
    }
}
