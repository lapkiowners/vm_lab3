package Errors;

public class ImpossibleToPutDirtyVectorException extends MatrixException {

    private String vectorName;

    public ImpossibleToPutDirtyVectorException(String vectorName) {
        this.vectorName = vectorName;
    }

    @Override
    public String getMessage() {
        return "Невозможно разместить вектор " + vectorName;
    }
}
