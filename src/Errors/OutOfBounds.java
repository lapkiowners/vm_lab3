package Errors;

public class OutOfBounds extends MatrixException{
    @Override
    public String getMessage() {
        return "Индекс вышел за границы строки!";
    }
}
