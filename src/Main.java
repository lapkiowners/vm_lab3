import main.lab.third.justTry.MatrixSolution.Matrix;
import main.lab.third.justTry.MatrixSolution.MatrixRow;
import main.lab.third.justTry.SystemModel;
import main.lab.third.justTry.util.random.RandomDoubleGenerator;
import main.lab.third.justTry.util.random.RandomGenerator;

import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        RandomGenerator generator = new RandomDoubleGenerator(1, 10);

        int indexP = 3;
        SystemModel model= new SystemModel(generator, 10, indexP);


        int matrixP = indexP + 1;
        int matrixQ = matrixP + 2;
        Matrix matrix = new Matrix(model.getMatrix());
        matrix.printMatrix();

        for(int i = 1 ; i < matrixP ; i ++) {
            double coefficient = 1 / matrix.get(i, i);

            matrix.mulRow(i, coefficient);


            matrix.addRow(i, matrixP, -matrix.get(matrixP, i));
            matrix.addRow(i, matrixQ, -matrix.get(matrixQ, i));
            if(i + 1 != matrixP)
                matrix.addRow(i, i + 1, -matrix.get(i + 1, i));
            matrix.printMatrix();
        }
        for(int i = matrix.getRows() ; i > matrixQ ; i --) {
            double coefficient = 1 / matrix.get(i, i);

            matrix.mulRow(i, coefficient);


            matrix.addRow(i, matrixP, -matrix.get(matrixP, i));
            matrix.addRow(i, matrixQ, -matrix.get(matrixQ, i));
            if(i - 1 != matrixQ)
                matrix.addRow(i, i - 1, -matrix.get(i - 1, i));
            matrix.printMatrix();
        }





        matrix.printMatrix();

        matrix.mulRow(matrixP, 1/matrix.get(matrixP,matrixP));
        matrix.addRow(matrixP, matrixP + 1, -matrix.get(matrixP + 1, matrixP));
        matrix.addRow(matrixP, matrixP + 2, -matrix.get(matrixP + 2, matrixP));

        matrix.printMatrix();

        matrix.mulRow(matrixP + 1, 1/matrix.get(matrixP + 1,matrixP + 1));
        matrix.addRow(matrixP + 1, matrixP, -matrix.get(matrixP, matrixP + 1));
        matrix.addRow(matrixP + 1, matrixQ, -matrix.get(matrixQ, matrixP + 1));

        matrix.printMatrix();

        matrix.mulRow(matrixQ, 1/matrix.get(matrixQ,matrixQ));
        matrix.addRow(matrixQ, matrixP, -matrix.get(matrixP, matrixQ));
        matrix.addRow(matrixQ, matrixP + 1, -matrix.get(matrixP + 1, matrixQ));

        matrix.printMatrix();

        for(int i = matrixP ; i > 1 ; i --) {
            double coefficient = -matrix.get(i - 1, i);

            matrix.addRow(i, i - 1, coefficient);

            matrix.printMatrix();
        }

        for(int i = matrixQ ; i < matrix.getRows(); i ++) {
            double coefficient = -matrix.get(i + 1, i);

            matrix.addRow(i, i + 1, coefficient);

            matrix.printMatrix();
        }

    }
}
